#include <iostream>
#include <vector>

typedef std::vector<int> int_v;

void Hanoi(int disks, int_v &source, int_v &auxiliary, int_v &destination);
void ShowTowers(int_v &source, int_v &auxiliary, int_v &destination);
int disks = 9;
int_v g_source(disks);
int_v g_destination(0);
int_v g_auxiliary(0);

int main()
{
	
	int i = 0;

	for (int i = 0; i < disks; i++)
	{
		g_source[i] = i;
	}

	std::cout << "Before:\n\n";
	ShowTowers(g_source, g_auxiliary, g_destination);
	
	Hanoi(disks, g_source, g_auxiliary, g_destination);

	std::cout << "After:\n\n";
	ShowTowers(g_source, g_auxiliary, g_destination);

	
	return 0;
}

void Hanoi(int disks, int_v &source, int_v &auxiliary, int_v &destination)
{
	if (disks > 0)
	{
		Hanoi(disks - 1, source, destination, auxiliary);
		destination.push_back(source.back());
		source.pop_back();
		ShowTowers(g_source, g_auxiliary, g_destination);
		Hanoi(disks - 1, auxiliary, source, destination);
	}
		
}

void write(const char *symbol)
{
	std::cout << "\n";
	for (int i = 0; i < disks; ++i)
		std::cout << *symbol;
	std::cout << "\n";
}

void ShowTowers(int_v &source, int_v &auxiliary, int_v &destination)
{
	system("cls");
	std::cout << "Source:";
	write("-");
	for (auto element : source)
		std::cout << element;
	write("=");


	std::cout << "Auxiliary:";
	write("-");
	for (auto element : auxiliary)
		std::cout << element;
	write("=");


	std::cout << "Destination:";
	write("-");
	for (auto element : destination)
		std::cout << element;
	write("=");
	std::cout << "\n\n";
	system("pause");
}