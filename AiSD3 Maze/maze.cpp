#include <iostream>
#include <vector>

/*
0 - empty
1 - wall
2 - explored
3 - end
*/

std::vector<std::vector<int>> maze = { {
	{ 3,0,0,0,0 },
	{ 1,1,1,1,0 },
	{ 1,0,0,0,0 },
	{ 0,0,1,1,1 },
	{ 0,1,0,0,0 },
	{ 0,0,0,1,0 },
} };

bool Path(int x, int y);

int main()
{
	Path(maze.size() - 1, maze[maze.size() - 1].size() - 1);

	for (int i = 0; i < maze[0].size() + 2; i++)
		std::cout << "#";
	std::cout << "\n";
	for (auto row : maze)
	{
		std::cout << "#";
		for (auto column : row)
		{
			switch (column)
			{
			case 0:
				std::cout << " ";
				break;
			case 1:
				std::cout << "#";
				break;
			case 2:
				std::cout << "-";
				break;
			case 3:
				std::cout << "!";
				break;
			default:
				break;
			}
		}
		std::cout << "#\n";
	}
	for (int i = 0; i < maze[0].size() + 2; i++)
		std::cout << "#";
	std::cout << "\n";

	system("pause");
}

bool Path(int x, int y)
{
	if (x < 0 || x >= maze.size() || y < 0 || y >= maze[x].size())
		return false;
	if (maze[x][y] == 1 || maze[x][y] == 2)
		return false;
	if (maze[x][y] == 3)
		return true;
	maze[x][y] = 2;
	if (Path(x + 1, y) == true)
	{
		maze[x][y] = 2;
		return true;
	}
	if (Path(x - 1, y) == true)
	{
		maze[x][y] = 2;
		return true;
	}
	if (Path(x, y + 1) == true)
	{
		maze[x][y] = 2;
		return true;
	}
	if (Path(x, y - 1))
	{
		maze[x][y] = 2;
		return true;
	}

	return false;
}
